"""
Runs FARFAR2 on a specified sequence from a specified row in a TSV file
The row index is provided by a SLURM batch array number.
"""
import os
import pandas as pd
import sys


def _call_farfar2(sequence_name, sequence, sec_struct, struct_count, std_out_dir, results_dir):
    """
    Issues the system call to FARFAR2 in Rosetta on DCC

    :param sequence_name: name of sequence
    :param sequence: RNA sequence string
    :param sec_struct: secondary structure string
    :param struct_count: number of models to create for given sequence
    :param std_out_dir: directory path to log STDOUT output
    :param results_dir: directory path to store FARFAR2 .out file
    :return: file path to the created .out file
    """
    out_file = "%s/%s.out" % (results_dir, sequence_name)

    #  input to farfar2:
    #  - sequence name
    #  - RNA sequence
    #  - secondary structure
    #  - # of structures per sequence
    farfar_cmd_list = [
        'rna_denovo.mpi.linuxgccrelease',
        '-minimize_rna true',
        '-out:nstruct %d' % struct_count,
        '--use_legacy_job_distributor=True',
        '--sequence=%s' % sequence,
        '--secstruct="%s"' % sec_struct,
        '-out:file:silent %s' % out_file,
        '> %s/%s_farfar.log' % (std_out_dir, sequence_name)
    ]
    farfar_cmd = " ".join(farfar_cmd_list)

    os.system(farfar_cmd)

    return out_file


def _call_extract_pdbs(sequence_name, silent_out_path, std_out_dir, pdb_dir):
    """
    Issues the system call to extract_pdbs in Rosetta on DCC. A .pdb file
    will be created for every structure within the given silent .out file.

    :param sequence_name: name of sequence
    :param silent_out_path: file path to the silent .out file (created by FARFAR2)
    :param std_out_dir: directory path to log STDOUT output
    :param pdb_dir: directory path & prefix to store output .pdb files
    :return: file path to the created .out file
    """
    #  input to extract_pdbs:
    #  - silent .out file generated from FARFAR2
    #  - prefix for the output .pdb file
    sys_cmd_list = [
        'extract_pdbs.mpi.linuxgccrelease',
        '-in:file:silent %s' % silent_out_path,
        '-out:prefix %s/%s_' % (pdb_dir, sequence_name),
        '> %s/%s_extract_pdbs.log' % (std_out_dir, sequence_name)
    ]
    sys_cmd = " ".join(sys_cmd_list)

    os.system(sys_cmd)


def run_farfar(tsv_file_in, row_index, silent_out_dir, pdb_dir, std_out_dir, struct_per_sequence=1):
    """
    Given a TSV file and a row index, runs FARFAR2 on the sequence in the row
    :param tsv_file_in: TSV file containing sequence data
    :param row_index: index of the row containing the sequence to process
    :param silent_out_dir: directory path to store FARFAR2 .out file
    :param pdb_dir: directory path to store .pdb files
    :param std_out_dir: directory path to log STDOUT output
    :param struct_per_sequence: number of models for FARFAR2 to create
    :return: None
    """
    df = pd.read_csv(tsv_file_in, delimiter="\t")

    # first we'll check that the sequence names are unique since we'll
    # be using them for the output file names
    unique_names = df['grna_name'].unique()
    if len(unique_names) != df.shape[0]:
        raise ValueError("Sequence names must be unique")

    # get row to process
    row = df.iloc[row_index]

    sequence_name = row['grna_name']
    sequence = row['rna_sequence']
    sec_struct = row['sec_struct']

    silent_out_path = _call_farfar2(
        sequence_name,
        sequence,
        sec_struct,
        struct_per_sequence,
        std_out_dir,
        silent_out_dir
    )

    _call_extract_pdbs(sequence_name, silent_out_path, std_out_dir, pdb_dir)


if __name__ == '__main__':
    tsv_file_in_arg = sys.argv[1]
    structures_per_sequence = int(sys.argv[2])
    silent_out_dir_arg = sys.argv[3]
    pdb_dir_arg = sys.argv[4]
    std_out_dir_arg = sys.argv[5]

    # the sequence to process is provided by the SLURM array task ID
    # but array task IDs are generally indexed at 1 so subtract 1
    sequence_row = int(os.environ['SLURM_ARRAY_TASK_ID']) - 1

    run_farfar(
        tsv_file_in_arg,
        sequence_row,
        silent_out_dir_arg,
        pdb_dir_arg,
        std_out_dir_arg,
        struct_per_sequence=structures_per_sequence
    )
