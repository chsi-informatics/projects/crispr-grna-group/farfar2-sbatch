#!/bin/bash
#SBATCH -c 1
module load Python/3.8.1
module load Rosetta/3.11
# args are in order:
#  $1 -> TSV file
#  $2 -> # of models to create for sequence
#  $3 -> directory to store FARFAR2 .out files
#  $4 -> directory to store .pdb fies
#  $5 -> directory to log FARFAR2 STDOUT output
python run_sequence_in_farfar2.py $1 $2 $3 $4 $5
