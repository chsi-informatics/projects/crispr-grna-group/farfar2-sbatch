#!/usr/bin/env bash

# check that we have 1 input argument
if [[ $# -ne 1 ]] ; then
    echo 'USAGE:' ${0} 'RUN_DIR'
    exit 1
fi

RUN_DIR=$1
# OUT_DIR=$2
# NUM_SIMUL_JOBS=$3
# 

# RUN_DIR="/work/josh/crispr/output/farfar2_out/test_with_pdb_10"
NUM_SIMUL_JOBS=1000

#----------------------------------------------------------------------------
# Do Not Modify Anything Below Here
#----------------------------------------------------------------------------
LOG_DIR="${RUN_DIR}/rna_align_logs"
rm -rf $LOG_DIR
mkdir -p $LOG_DIR
ALL_PDBS=($RUN_DIR/pdb/*.pdb)
# ls $CUR_DIR/*.pdb

NUM_FILES=${#ALL_PDBS[@]}
# NUM_FILES=5

echo $NUM_FILES

# NUM_COMPARE=$(( $NUM_FILES*$NUM_FILES))
# echo $NUM_COMPARE
ARRAY_MAX=$((NUM_FILES-1))

# for CUR_COMPARE in $(seq 0 $(($NUM_COMPARE-1)))
sbatch --output ${LOG_DIR}/slurm_%j.out -a 0-${ARRAY_MAX}%${NUM_SIMUL_JOBS} -p chsi -A chsi ./RNAalign_sbatch.sh $RUN_DIR
