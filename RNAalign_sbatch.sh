#!/usr/bin/env bash
#SBATCH -c 1
# args are in order:
#  $1 -> directory containing run results (parent directory of pdb directory)

RUN_DIR=$1

ALL_PDBS=($RUN_DIR/pdb/*.pdb)

for CUR_PDB in "${ALL_PDBS[@]}"; do
  RNAalign -outfmt 2 ${ALL_PDBS[${SLURM_ARRAY_TASK_ID}]} ${CUR_PDB}
done
