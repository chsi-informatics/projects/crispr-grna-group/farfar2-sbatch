# farfar2-sbatch

## Run

### Run wrapper script (easy way)

The wrapper shell script will automatically determine the number of jobs to run and create the output directories. It takes the following arguments:

- TSV file
- output directory
- number of simultaneous jobs

When finished, the output directory will contain 3 subdirectories: `silent_out`, `pdb`, and `log` containing the silent .out files, .pdb files, and the log files respectively. Below is an example that runs 50 simultaneous jobs:

`run_farfar2.sh /path/to/example.tsv /path/to/output_dir 50`

**Note that the number of simultaneous jobs is used to avoid filling up the entire partition queue. This number should probably be set to several hundred jobs or fewer.**

### Run sbatch command directly (slightly more difficult)

The included shell script `farfar2-sbatch.sh` takes 4 arguments:

- TSV file
- number of models to create for sequence
- directory to store FARFAR2 .out results (create prior to running)
- directory to store .pdb files (create prior to running)
- directory to log FARFAR2 STDOUT output (create prior to running)

Below is an example sbatch command to run FARFAR2 on a TSV file with 500 rows (sequences) with 1 model per sequence and limit to 80 simultaneous jobs:

`sbatch -a 1-500%80 -p chsi farfar2-sbatch.sh /path/to/example.tsv 1 results log`

**Note the use of the % to limit the number of simultaneous jobs.  This avoids filling up the entire queue.**

### Run RNAalign on PDB files

The python script `pairwise_pdb_align.py` runs RNAalign on the pairwise combinations of .pdb files in a given directory and saves the concatenated results to a CSV file. Example usage:

`pairwise_pdb_aligny.py /path/to/pdbs output.csv`

**Note: The `RNAalign` utility is not available on DCC but has been added to `/hpc/group/chsi/bin`. Add this to your PATH environment variable to run this Python script.**



# Commandline Examples for sbatch array
```
# cd to top level of repository
./RNAalign_run.sh /work/josh/crispr/output/farfar2_out/test_with_pdb_10
```

When the above sbatch array has finished, run the following
```
./RNAalign_combine.sh /work/josh/crispr/output/farfar2_out/test_with_pdb_10
```

```
srun -A chsi -p chsi -c2 --mem=100G singularity exec --bind /work:/work /work/josh/crispr/images/secondary-structure-singularity_v007.sif Rscript -e "rmarkdown::render('make_3dstructure_umap.Rmd')" 
# params=list(hts_reads=4372,debug=FALSE))"
```

