# check that we have 3 input arguments
if [[ $# -ne 3 ]] ; then
    echo 'USAGE:' ${0} '/path/to/tsv_file /path/to/output_dir #_simultaneous_jobs'
    exit 1
fi

IN_TSV=$1
OUT_DIR=$2
NUM_SIMUL_JOBS=$3

if [ ! -f $IN_TSV ]; then
    echo "File '$IN_TSV' not found!"
    exit 1
fi

# change as needed
NUM_MODELS=1

NUM_LINES=$(wc -l < "$IN_TSV")
ARRAY_MAX=$((NUM_LINES-1))

echo "Number of gRNAs to run: ${ARRAY_MAX}"

mkdir -p ${OUT_DIR}/silent_out ${OUT_DIR}/pdb ${OUT_DIR}/log
sbatch --output ${OUT_DIR}/log/slurm_%j.out -a 1-${ARRAY_MAX}%${NUM_SIMUL_JOBS} -p chsi -A chsi ./farfar2-sbatch.sh ${IN_TSV} ${NUM_MODELS} ${OUT_DIR}/silent_out ${OUT_DIR}/pdb ${OUT_DIR}/log
# sbatch --output ${OUT_DIR}/log/slurm_%j.out -a 1-${ARRAY_MAX} -p chsi -A chsi ./farfar2-sbatch.sh ${IN_TSV} ${NUM_MODELS} ${OUT_DIR}/silent_out ${OUT_DIR}/pdb ${OUT_DIR}/log
