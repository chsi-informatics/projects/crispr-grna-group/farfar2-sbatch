"""
Run RNA align utility on all pairwise combinations of given directory of .pdb files
"""
import glob
import itertools
from io import StringIO
import os
import pandas as pd
import sys
from subprocess import PIPE, run


def run_pairwise_rna_align(pdb_dir, csv_file_out):
    """
    Run RNA align utility on all pairwise combinations of given directory of .pdb files

    :param pdb_dir: directory path containing .pdb files
    :param csv_file_out file path to store output CSV file
    :return: None
    """
    pdb_files = sorted(glob.glob(os.path.join(pdb_dir, '*.pdb')))

    pair_wise_combos = sorted(list(itertools.combinations(pdb_files, 2)))

    df_output_list = []

    for (pdb1, pdb2) in pair_wise_combos:
        sys_cmd = 'RNAalign -outfmt 2 %s %s' % (pdb1, pdb2)

        sys_cmd_result = run(sys_cmd, stdout=PIPE, stderr=PIPE, universal_newlines=True, shell=True)
        output = sys_cmd_result.stdout

        output_io = StringIO(output)
        df_output = pd.read_csv(output_io, sep='\t', nrows=1)

        df_output_list.append(df_output)

    df_concat = pd.concat(df_output_list)

    # clean up a few things
    df_concat.rename(columns={'#PDBchain1': 'PDBchain1'}, inplace=True)
    df_concat.PDBchain1 = df_concat.PDBchain1.map(os.path.basename)
    df_concat.PDBchain2 = df_concat.PDBchain2.map(os.path.basename)

    df_concat.to_csv(csv_file_out, index=False)


if __name__ == '__main__':
    pdb_dir_arg = sys.argv[1]
    csv_out_file_arg = sys.argv[2]

    run_pairwise_rna_align(pdb_dir_arg, csv_out_file_arg)
