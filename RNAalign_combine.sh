#!/usr/bin/env bash

# args are in order:
#  $1 -> directory containing run results (parent directory of pdb directory)

RUN_DIR=$1

egrep --no-filename -v "PDBchain1|Total CPU time is" ${RUN_DIR}/rna_align_logs/slurm*.out > ${RUN_DIR}/pairwise_distances.tsv
